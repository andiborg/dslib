<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
session_start();

if (!$_GET[patientTempId]) {
	header('HTTP/1.1 400 Invalid request.');
	echo("Patient temp id is missing.'");
}

$rSession = getRemoteSessionFactory()->loadByTempId($_GET[patientTempId]);

$patientId = $rSession->getId($_GET['patientTempId']);
if (!$patientId) {
	header('HTTP/1.1 400 Invalid request.');
	echo("Unknown patient temp id '{$_GET['patientTempId']}'.");
	return;
}
$physicianId = $rSession->getId($_GET['physicianTempId']);
if (!$physicianId) {
	header('HTTP/1.1 400 Invalid request.');
	echo("Unknown physician temp id '{$_GET['physicianTempId']}'.");
	return;
}

$_SESSION['patientdiagnoses'][$patientId][] =
	array('result' => $_GET['diagnosis'], 'physicianId' => $physicianId);

$newPatientTempId = $rSession->getTempId($patientId);
$rSession->save();

header("Location: /dslib/demos/php/idat/main.php?patientTempId=$newPatientTempId");
