<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
session_start();

header('Access-Control-Allow-Origin: http://imflrene:82');
header('Access-Control-Max-Age: '.(10));
header('Access-Control-Allow-Methods: PUT, DELETE');
header('Access-Control-Allow-Headers: x-requested-with');

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS')
	exit();

if (!isset($_SESSION['patientdiagnoses']))
	$_SESSION['patientdiagnoses'] = array(
		1 => array( array('result' => 'krank', 'physicianId' => 10),
			array('result' => 'echt krank', 'physicianId' => 11)),
		2 => array( array('result' => 'gesund', 'physicianId' => 11))
	);

$rSession = getRemoteSessionFactory()->loadByTempId($_GET['patientTempId']);
if ($rSession === null)
	echoAndExit("Die Sitzung ist abgelaufen. Bitte melden Sie sich ab und erneut an.");

$patientId = $rSession->getId($_GET['patientTempId']);
$sessionId = $rSession->getSessionId();
if ($patientId === null)
	echoAndExit("Unbekannter Patient (TempId: {$_GET['patientTempId']}).");

$output = <<<OUTPUT
<form name="diagnosis" action="/dslib/demos/php/mdat/addDiagnosis.php">
<table width="500px">
<thead style="background:lightgrey;"><tr><td width="40%">Diagnose</td><td width="60%">Arzt</td></tr></thead>
<tbody>
OUTPUT;

foreach ($_SESSION['patientdiagnoses'][$patientId] as $diagnosis) {
	$physicianTempId = $rSession->getTempId($diagnosis['physicianId']);
	$output .= <<<OUTPUT
	<tr><td>{$diagnosis['result']}</td>
		<td data-subject="physicianName" data-tempid="{$physicianTempId}"/></tr>
OUTPUT;
}

$output .= <<<OUTPUT
	<tr><td><input type="text" name="diagnosis"/></td><td/></tr>
</table>
<input type="hidden" name="patientTempId" value="{$_GET['patientTempId']}" />
<input type="hidden" name="physicianTempId" value="{$_GET['physicianTempId']}" />
<br/>
<input type="submit" />
</form>
OUTPUT;

$rSession->save();
echoAndExit($output);

function echoAndExit($output) {
	if (!empty($_GET['callback']))
		echo ($_GET['callback']."(".json_encode($output).");");
	else
		echo $output;
	exit();
}
