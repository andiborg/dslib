<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
require_once('remoteSession/remoteSessionReceivers.php');

$remoteSessionReceiver = new HttpRemoteSessionReceiver(getRemoteSessionFactory());
$remoteSessionReceiver->receive();
