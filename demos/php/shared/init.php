<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

set_include_path('.'.PATH_SEPARATOR.
	'../../../lib/server/php'.PATH_SEPARATOR.
	get_include_path());

require_once 'remoteSession/remoteSession.php';
require_once 'remoteSession/remoteSessionFactory.php';
require_once 'remoteSession/remoteSessionReceivers.php';
require_once 'remoteSession/remoteSessionSenders.php';
require_once 'remoteSession/remoteSessionStores.php';

session_name($config['sessionName']);

function getRemoteSessionFactory() {
	global $config;
	$remoteSessionDir = $config['remoteSessionDir'];
	if (!file_exists($remoteSessionDir))
		mkdir($remoteSessionDir);
	return new remoteSessionFactory(
		new FileRemoteSessionStore($remoteSessionDir, 300, true),
		new HttpRemoteSessionSender($config['remoteSessionConnHost'],
			$config['remoteSessionConnPort'], $config['remoteSessionConnPath'])
	);
}
