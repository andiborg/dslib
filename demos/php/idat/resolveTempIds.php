<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
session_start();
$rSessionFactory = getRemoteSessionFactory();
$rSession = null;

if (!isset($_GET['data'])) {
	header('HTTP/1.1 400 Invalid request.');
	return;
}

function resolve($subject, $tempId) {
	global $rSessionFactory;
	global $rSession;

	if ($rSession === null)
		$rSession = $rSessionFactory->loadByTempId($tempId);
	if ($rSession === null)
		return '';

	$id = $rSession->getId($tempId);
	if ($id === null)
		return '';

	switch ($subject) {
		case 'physicianName':
			$physicianName = $_SESSION['physicians'][$id]['name'];
			return ($physicianName !== null ? $physicianName : '');
		default:
			return '';
	}
}

$data = json_decode($_GET['data'], true);
$result = array();

if (isset($data['subjects']) && is_array($data['subjects']))
	foreach ($data['subjects'] as $subject => $tempIds)
		if (is_array($tempIds))
			foreach ($tempIds as $tempId)
				$result[$subject][$tempId] = resolve($subject, $tempId);

if (isset($data['tempIds']) && is_array($data['tempIds']))
	foreach ($data['tempIds'] as $tempId => $subjects)
		if (is_array($subjects))
			foreach ($subjects as $subject)
				$result[$subject][$tempId] = resolve($subject, $tempId);

echo json_encode($result);
