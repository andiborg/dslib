<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

set_include_path('.'.PATH_SEPARATOR.
	'../shared'.PATH_SEPARATOR.
	'../../../lib/server/php'.PATH_SEPARATOR.
	get_include_path());

$config = array(
	'sessionName' => 'dslib_php_demo_idat_session',
	'remoteSessionConnHost' => 'localhost',
	'remoteSessionConnPort' => 80,
	'remoteSessionConnPath' => '/dslib/demos/php/mdat/receiveTempIds.php',
	'remoteSessionConnPwd' => 'w39875hgs37n3v74523o57own3ksbkvb6tmj35jk375i8347537',
	'remoteSessionDir' => sys_get_temp_dir().'/dslib_php_demo_idat_session_store'
);
