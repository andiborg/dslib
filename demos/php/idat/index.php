<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
session_start();
$rSessionFactory = getRemoteSessionFactory();

if ($_GET['logout']) {
	$rSession = $rSessionFactory->load(session_id());
	if ($rSession !== null) {
		$rSession->destroy();
		$rSession = null;
	}

	setcookie(session_name(), '');
	session_unset();
	session_destroy();
}
else if ($_POST['name'] && $_POST['password']) {
	$rSession = $rSessionFactory->load(session_id());
	if ($rSession !== null) {
		$rSession->destroy();
		$rSession = null;
	}
	$rSession = $rSessionFactory->create(session_id());

	session_unset();
	$_SESSION['authenticated'] = true;
	$_SESSION['user_name'] = $_POST['name'];
	$physicianId = rand(20, 1000);
	$_SESSION['physicianId'] = $physicianId;

	$_SESSION['patients'] = array(
		1 => array( 'name' => "Heinz Heinzen" ),
		2 => array( 'name' => "Peter Peterson" )
	);
	$_SESSION['physicians'] = array(
		10 => array( 'name' => "Markus Müller" ),
		11 => array( 'name' => "Gustav Gans" )
	);
	$_SESSION['physicians'][$physicianId] = array( 'name' => $_SESSION['user_name'] );
}

if (!empty($_SESSION['authenticated']))
	header('Location: main.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>DSLIB Demo</title>
<link rel="stylesheet" href="../shared/demo.css" />
</head>
<body>
	<h2>DSLIB Demo</h2>
	Sie sind nicht angemeldet.<br/>
	Bitte melden Sie sich mit beliebigem Benutzernamen und Passwort an.<br/><br/>
	<form action="index.php" method="post">
		Name: <input type="text" name="name" />
		Passwort: <input type="password" name="password" />
		<input type="submit" value="Submit" />
	</form>
</body>
</html>
