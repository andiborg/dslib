<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once('config.php');
require_once('init.php');
session_start();
$rSession = getRemoteSessionFactory()->load(session_id());
if (!$_SESSION['authenticated'] || $rSession === null) {
	header('Location: index.php?logout=true');
	exit;
}
?>
<!DOCTYPE html>
<html style="height: 95%; width: 95%;">
<head>
<title>DSLIB Demo</title>
	<meta charset="utf-8">
	<script src="../../../external/js/jquery.js"></script>
	<script src="../../../external/js/jquery.jsonp.js"></script>
	<script src="../../../external/js/json2.js"></script>
	<script src="../../../lib/client/js/netApiConnection.js"></script>
	<script src="../../../lib/client/js/defaultResolver.js"></script>
	<script src="../../../lib/client/js/tempIdCache.js"></script>
	<script src="../../../lib/client/js/tempIdResolver.js"></script>
	<link rel="stylesheet" href="../shared/demo.css" />
</head>
<body style="height: 100%; width: 100%;">
<div class="header">
	Angemeldet als: <?php echo $_SESSION['user_name']?>&nbsp;&nbsp;&nbsp;
	<div style="float:right;"><a href="index.php?logout=true">Abmelden</a></div>
</div>
<div class="menu">
<?php
	foreach ($_SESSION['patients'] as $patientId => $patient) {
		$tempId = $rSession->getTempId($patientId);
		echo "\t".'<a class="item" href=";" onclick="loadPatient(\''.$patient['name'].'\', \''.$tempId.'\'); return false;">'.
			$patient['name']."</a><br/>\n";
	}
?>
</div>

<div class="content"> 
	<div id="patientNameDiv" style="font-size: 140%;"></div><br/>
	<div id="patientDataDiv"></div>
</div>

<script>
	var physicianTempId = '<?php echo $rSession->getTempId($_SESSION['physicianId']); ?>';

	mdat = netApiConnectionConstructor({ baseUrl: "//localhost/dslib/demos/php/mdat/" });

	function loadPatient(patientName, patientTempId) {
		mdat.ajax({
			url: 'patientData.php',
			data: {patientTempId: patientTempId, physicianTempId: physicianTempId},
			dataType: 'html',
			success: function(data, textStatus, xhr) {
				$('#patientNameDiv').html(patientName);
				$('#patientDataDiv').html(data);
				tempIdResolver.resolve();
			},
			error: function(xhr, textStatus, errorThrown) {
			}
		});
	}

	tempIdResolver = tempIdResolverConstructor();

<?php
	if (isset($_GET['patientTempId'])) {
		$patientId = $rSession->getId($_GET['patientTempId']);
		$newPatientTempId = $rSession->getTempId($patientId);
		echo("loadPatient('{$_SESSION['patients'][$patientId]['name']}', '$newPatientTempId');");
	}
	$rSession->save();
?>
</script>

</body>
</html>
