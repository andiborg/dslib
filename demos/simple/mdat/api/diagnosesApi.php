<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once 'remoteSession/remoteSession.php';
require_once 'remoteSession/remoteSessionFactory.php';
require_once 'remoteSession/remoteSessionStores.php';

class DiagnosesApi {
	public static function getDiagnosis(&$request, $patientTempId) {
		$remoteSession = self::getRemoteSession();
		$id = $remoteSession->getId($patientTempId);
		$request->setOutput(self::$diagnoses[$id]);
	}

	// Private functions.

	private static function getRemoteSession() {
		$sessionFactory = new RemoteSessionFactory(new SessionRemoteSessionStore());
		$remoteSession = $sessionFactory->load('fakeRemoteSession');
		if ($remoteSession === null)
			$remoteSession = $sessionFactory->create('fakeRemoteSession');
		return $remoteSession;
	}

	// Fake data initialisation.

	public static function init() {
		self::$diagnoses = array(
			1 => 'krank',
			2 => 'gesund'
		);
	}

	private static $diagnoses;
}

DiagnosesApi::init();

?>
