<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

require_once 'remoteSession/remoteSession.php';
require_once 'remoteSession/remoteSessionFactory.php';
require_once 'remoteSession/remoteSessionStores.php';
require_once 'remoteSession/idReplacer.php';

class PatientsApi {

	public static function getPatients(&$request) {
		if (empty($_SESSION['authenticated'])) {
			$request->setError(100, 'Authentication required.');
			return;
		}

		$patients = self::$patients;
		$remoteSession = self::getRemoteSession();
		$patients = IdReplacer::replaceWithTempIds($remoteSession, true, $patients);
		$request->setOutput($patients);
		$remoteSession->save();
	}

	public static function getTempId(&$request, $patientId) {
		if (empty($_SESSION['authenticated'])) {
			$request->setError(100, 'Authentication required.');
			return;
		}

		$remoteSession = self::getRemoteSession();
		$request->setOutput($remoteSession->getTempId($patientId));
		$remoteSession->save();
	}

	// Private functions.

	private static function getRemoteSession() {
		$sessionFactory = new RemoteSessionFactory(new SessionRemoteSessionStore());
		$remoteSession = $sessionFactory->load('fakeRemoteSession');
		if ($remoteSession === null)
			$remoteSession = $sessionFactory->create('fakeRemoteSession');
		return $remoteSession;
	}

	// Fake data initialisation.

	public static function init() {
		self::$patients = array(
			array( 'id' => 1, 'name' => "Heinz Heinzen" ),
			array( 'id' => 2, 'name' => "Peter Peterson" )
		);
	}

	private static $patients;
}

PatientsApi::init();
