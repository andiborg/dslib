<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

class AuthenticationApi {

	public static function login(&$request) {
		$_SESSION['authenticated'] = true;
		$_SESSION['username'] = $request->getParam('username');
		$request->setOutput(true);
	}

	public static function logout(&$request) {
		session_destroy();
		$request->setOutput(true);
	}

	public static function ping(&$request) {
		if (empty($_SESSION['authenticated'])) {
			$request->setError(100, 'Authentication required.');
			return;
		}

		$request->setOutput('pong');
	}

	public static function whoAmI(&$request) {
		if (empty($_SESSION['authenticated'])) {
			$request->setError(100, 'Authentication required.');
			return;
		}

		$request->setOutput($_SESSION['username']);
	}

}
