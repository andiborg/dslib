<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

//error_reporting(E_ALL | E_STRICT);

set_include_path('.'.PATH_SEPARATOR.
	'../../../../lib/server/php'.PATH_SEPARATOR.
	get_include_path());

require_once "restApi/request.php"; 
require_once "restApi/requestException.php";
require_once "restApi/requestHandler.php";
require_once 'authenticationApi.php';
require_once 'patientsApi.php';

$main = new Main();
$main->run();

function debugError() {
	
}

class Main {

	public function run() {
		try {
			session_name('dslib_simple_demo_session');
			session_start();

			header("Cache-Control: no-cache, must-revalidate");
			header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

			$request = new Request();
			$request->fromContext(5);

			$requestHandler = new RequestHandler();
			$requestHandler->addFunction('get', '', array($this, 'help'), array(&$requestHandler));
			$requestHandler->addFunction('get', 'ping', array($this, 'ping'));
			$requestHandler->addFunction('get', 'auth/login', array('AuthenticationApi', 'login'));
			$requestHandler->addFunction('get', 'auth/logout', array('AuthenticationApi', 'logout'));
			$requestHandler->addFunction('get', 'auth/ping', array('AuthenticationApi', 'ping'));
			$requestHandler->addFunction('get', 'auth/whoami', array('AuthenticationApi', 'whoAmI'));
			$requestHandler->addFunction('get', 'patients/1/tempId', array('PatientsApi', 'getTempId'), 1);
			$requestHandler->addFunction('get', 'patients/2/tempId', array('PatientsApi', 'getTempId'), 2);
			$requestHandler->addFunction('get', 'patients', array('PatientsApi', 'getPatients'));

			if (!$requestHandler->handleRequest($request, $request->getSubUrl(0)))
				$request->setHttpError(400, 'Unknown function.');
		}
		catch (RequestException $reqException) {
			$request->setHttpError($reqException->getHttpStatusCode(), $reqException->getMessage());
		}
		catch (Exception $exception) {
			$request->setHttpError(500, $exception->getMessage());
		}

		try {
			$request->echoOutput();
		}
		catch (Exception $exception)
		{
			header('HTTP/1.1 500 '.Request::getStatusCodeMessage(500));
			echo $exception->getMessage();
		}
	}

	public function help(&$request, &$requestHandler) {
		$handlerStr = '';
		$handlers = $requestHandler->getHandlers();
		foreach ($handlers as $handler)
			if ($handler['httpMethod'] === 'get')
				$handlerStr .= '<a href="'.$handler['pattern'].'">'.strtoupper($handler['httpMethod']).' '.
					$handler['pattern']."</a>\n";
			else
				$handlerStr .= strtoupper($handler['httpMethod']).' '.$handler['pattern']."\n";

		$help = <<<HELP
<!DOCTYPE html><html>
<head>
	<meta charset="utf-8">
	<title>DSLib Beispielserver Hilfe</title>
	<style type="text/css">a { text-decoration:none }</style>
</head>
<body><pre>
DSLib IDAT-Beispielserver

Definierte Funktionen:
$handlerStr
</pre></body>
HELP;

		$request->setRawContentType('text/html');
		$request->setRawOutput($help);
	}

	public function ping(&$request) {
		$request->setOutput('pong');
	}
}
