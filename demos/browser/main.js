/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

ds = {};

function log(message) {
	if (console)
		console.log(message);
}

function main () {
	var idat = netApiConnectionConstructor({ baseUrl: "../simple/idat/api/" });
	var mdat = netApiConnectionConstructor({ baseUrl: "../simple/mdat/api/" });
	var auth = authorizationConstructor({ connection: idat });

	var loadPatient = function(patientName, patientTempId) {
		$.get('patientTemplate.html', function(template) {
			$('#content').html(template);
			mdat.getJSON('diagnoses/' + patientTempId, function(diagnosis) {
				$('#patientName').html(patientName);
				$('#patientDiagnosis').html(diagnosis);
			});
		});
	}

	var login = function(username, password) {
		auth.login(username, password, function(success) {
			if (success)
				showMain(username);
			else
				alert('Failed.');
		});
	};

	var logout = function() {
		auth.logout(function(success) {
			if (success)
				showLogin();
			else
				alert('Failed.');
		});
	};

	function showLogin() {
		$('#container').load('login.html');
	}

	function showMain() {
		$('#container').load('main.html', function() {
			auth.refreshUserInfo(function(userInfo) {
				$('#username').html(userInfo);
			});
			idat.getJSON('patients', function(data) {
				var directive = {
					'span': {
						'patient <- patients': {
							'a': 'patient.name',
							'a@onclick': 'ds.loadPatient(\'#{patient.name}\', \'#{patient.id}\'); return false;'
						}
					}
				};
				$('#patientsDiv').render({patients: data}, directive);
			});
		});
	}

	idat.getJSON('auth/ping', function(data) {
		if (data.__ERROR__ && data.__ERROR__.code == 100)
			showLogin();
		else
			showMain();
	});

	// Build ds:
	// Objects
	ds.auth = auth;
	
	// Functions
	ds.loadPatient = loadPatient;
	ds.login = login;
	ds.logout = logout;
}
