<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package RestApi
 */

/**
 * RequestException represents an Exception that can be returned to the client as an http error.
 */
class RequestException extends Exception {
	private $httpStatusCode;

	/**
	 * @param int $httpStatusCode
	 * @param string $exceptionMessage Message that may be sent to the client. Not that text after the status code!
	 */
	function __construct($httpStatusCode, $exceptionMessage) {
		parent::__construct($exceptionMessage);
		$this->httpStatusCode = $httpStatusCode;
	}

	function getHttpStatusCode() {
		return $this->httpStatusCode;
	}
}

?>
