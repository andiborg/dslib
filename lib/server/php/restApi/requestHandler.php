<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package RestApi
 */

require_once dirname(__FILE__).'/request.php';

/**
 * RequestHandler handles request using a list of functions and subhandlers.
 */
class RequestHandler {
	private $handlers = array();

	/**
	 * Adds a handling function to the list of handlers and functions.
	 * Order of the addFunction calls is relevant, as the handler will only try to match functions as long as a pattern
	 * matches. If a function pattern matches the request, the function must handle the request.
	 * @param string $httpMethod Http method, e.g. 'get' or '*' for all methods.
	 * @param string $pattern Url pattern that the function is called for. {0}, {1} match everything and will be used
	 * as parameters in the function call. e.g. 'auth/login/user/{1}/password/{0}' would use {0} as first and {1} as
	 * second extra parameter.
	 * @param array $callback Callback array as in the php function call_user_func_array.
	 * @param mixed $params Extra parameters that will be used before the pattern parameters.
	 * @see addHandler
	 */
	public function addFunction($httpMethod, $pattern, $callback, $params = null) {
		$this->internalAddHandler($httpMethod, $pattern, $callback, $params, false);
	}

	/**
	 * Adds a handler to the list of handlers and functions.
	 * As in {@link addFunction}, order of the calls is relevant. But a handler may return false when unable
	 * to handle a request. In this case the handler will continue matching the next handlers and functions.
	 * @param string $httpMethod Http method, e.g. 'get' or '*' for all methods.
	 * @param string $pattern Url pattern that the function is called for. {0}, {1} match everything and will be used
	 * as parameters in the function call. e.g. 'auth/login/user/{1}/password/{0}' would use {0} as first and {1} as
	 * second extra parameters.
	 * @param array $callback Callback array as in the php function call_user_func_array.
	 * @param mixed $params Extra parameters that will be used before the pattern parameters. 
	 * @see addFunction
	 */
	public function addHandler($httpMethod, $pattern, $callback, $params = null) {
		$this->internalAddHandler($httpMethod, $pattern, $callback, $params, true);
	}

	/**
	 * @return array Array of registered handlers in the internal format.
	 */
	public function getHandlers() {
		return $this->handlers;
	}

	/**
	 * Tries to handle the request with the first matching function or any subhandler.
	 * @return bool True, if the handler could handle the request, false otherwise.
	 */
	public function handleRequest(&$request, $url) {
		foreach ($this->handlers as &$handler) {
			if ($handler['httpMethod'] != '*' && $handler['httpMethod'] != $request->getHttpMethod())
				continue;

			$match = $this->matchPattern($handler['pattern'], $url);
			if ($match === null || (!$handler['isHandler'] && !empty($match['remainder'])))
				continue;

			$params = $match['params'];
			if (!($params === null)) {
				$callParams = array();
				$callParams[] = &$request;
				if ($handler['isHandler'])
					$callParams[] = $match['remainder'];
				if ($handler['params']) $callParams = array_merge($callParams, $handler['params']);
				$callParams = array_merge($callParams, $params);
				$result = call_user_func_array($handler['callback'], $callParams);
				if ($handler['isHandler'])
					return $result;

				return true;
			}
		}

		return false;
	}

	// Private Functions.

	private function buildRegExp($pattern) {
		$regexp = preg_replace('|\{(\d*)\}|', '(?P<p\1>[^/]*)', $pattern);
		$greedyCount = 0;
		$regexp = preg_replace('|\{(\d*)g\}|', '(?P<p\1>.*)', $regexp, -1, $greedyCount);
		$regexp = '|^'.$regexp.'|';

		return $regexp;
	}

	private function internalAddHandler($httpMethod, $pattern, $callback, $params, $isHandler) {
		$paramsArray = $params;
		if (isset($params) && !is_array($params)) {
			$paramsArray = array();
			$paramsArray[] = $params;
		}

		$this->handlers[] = array(
			'httpMethod' => $httpMethod, 'pattern' => $pattern, 'callback' => $callback, 'params' => $paramsArray,
			'isHandler' => $isHandler);
	}

	private function matchPattern($pattern, $url) {
		$regexp = $this->buildRegExp($pattern);

		$match = null;
		if (preg_match($regexp, $url, $matches)) {
			$match = array();
			$match['length'] = strlen($matches[0]);
			$match['remainder'] = substr($url, $match['length']);

			$params = array();
			foreach ($matches as $key => &$value) {
				if (substr($key, 0, 1) == 'p')
					$params[substr($key, 1)] = $value;
			}
			if (!ksort($params, SORT_NUMERIC))
				throw new Exception('Error sorting parameter list.');

			$match['params'] = $params;
		}

		return $match;
	}

	private function paramPos($pattern) {
		if (strlen($pattern) < 2 || $pattern{0} != '{' || $pattern{strlen($pattern) - 1} != '}')
			return null;

		$pos = (int)substr($pattern, 1, strlen($pattern) - 2);
		return $pos;
	}
}
