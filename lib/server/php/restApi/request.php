<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package RestApi
 */

/** */
require_once dirname(__FILE__).'/requestException.php';

/**
 * Request holds all data that is needed for the complete of handling a request and creating a response.
 */
class Request
{
	// Input
	private $data;
	private $httpAccept;
	private $jsonpCallback;
	private $httpMethod;
	private $url;

	// Output
	private $cacheControl = 'no-store, no-cache, must-revalidate';
	private $eTag;
	private $expire = 'Sat, 26 Jul 1997 05:00:00 GMT';
	private $rawContentType;
	private $error;
	private $httpError;
	private $output;
	private $rawOutput;

	// Public functions.

	/**
	 * Echoes the complete output to the client, including header fields and body content.
	 * This method is typically called after the request is processed and the output is set.
	 */
	public function echoOutput() {
		if ($this->cacheControl) header('Cache-Control: '.$this->cacheControl);
		if ($this->eTag) header('ETag: '.$this->eTag);
		if ($this->expire) header('Expires: '.$this->expire);

		if ($this->httpError) {
			$this->httpError['httpStatusMsg'] = $this->getStatusCodeMessage($this->httpError['httpStatusCode']);
			$this->echoRaw('text/plain', $this->httpError['msg'], false,
				$this->httpError['httpStatusCode'], $this->httpError['httpStatusMsg']);
		}
		else if ($this->error || $this->rawOutput === null) {
			if ($this->error)
				$output = array('__ERROR__' => $this->error);
			else
				$output = $this->output;

			$rawContent = null;
			$preferredContentType = $this->preferredContentType();
			switch ($preferredContentType['type']) {
				case 'json':
					$this->echoRaw($preferredContentType['name'], json_encode($output), true);
					break;
				case 'html':
				case 'text':
					if (is_string($output))
						$this->echoRaw($preferredContentType['name'], $output, false);
					else
						$this->echoRaw($preferredContentType['name'], json_encode($output), true);
					break;
				default:
					throw new Exception("Unknown content type '$preferredContentType'");
			}
		}
		else
			$this->echoRaw($this->rawContentType, $this->rawOutput, false);
	}

	/**
	 * Initializes the request object with data from the php context.
	 * This method is typically called before the request is processed to read and parse all relevant data
	 * from the php environment variables as needed.
	 * @param int $prefix Number of url parts to remove at the start of the url. Typically used to remove path parts
	 * such as '/api/....'.
	 */
	public function fromContext($prefix = 0) {
		if (!$_SERVER['REQUEST_METHOD'])
			throw new RequestException(400, 'REQUEST_METHOD missing.');
		if (!isset($_SERVER['REQUEST_URI']))
			throw new RequestException(400, 'REQUEST_URI missing.'); 

		$this->httpMethod = strtolower($_SERVER['REQUEST_METHOD']);
		if ($this->httpMethod === 'post')
			if ($this->getUrlParam('XDomainRequestMethod'))
				$this->httpMethod = strtolower($this->getUrlParam('XDomainRequestMethod'));

		$requestUrl = $_SERVER['REQUEST_URI'];
		$paramStart = strpos($requestUrl, '?');
		if ($paramStart !== false)
			$requestUrl = substr($requestUrl, 0, $paramStart);
		$this->url = explode('/', $requestUrl);
		array_splice($this->url, 0, 1 + $prefix);

		if (isset($_SERVER['HTTP_ACCEPT']))
			$this->httpAccept = $this->parseHttpAccept($_SERVER['HTTP_ACCEPT']);
		else
			$this->httpAccept = null;
		if (empty($this->httpAccept))
			$this->httpAccept[] = array('media-type' => '*', 'subtype' => '*', 'quality' => 1.0);

		if (!empty($_GET['callback']))
			$this->jsonpCallback = $_GET['callback'];
	}

	public function getRawContentType() {
		return $this->rawContentType;
	}

	public function getBody() {
		return file_get_contents("php://input");
	}

	public function getData() {
		if ($this->data)
			return $this->data;

		switch ($this->httpMethod)
		{
			case 'get':
				$this->data = $_GET;
				break;
			case 'post':
				$this->data = file_get_contents("php://input");
				break;
			default:
				break;
				//throw new RequestException(501, "Request type '".$this->httpMethod."' is not supported.");
		}

		return $this->data;
	}

	/**
	 * @return array Http accept header parsed into array elements that contain 'media-type', 'subtype' and 'quality'.
	 */
	public function getHttpAccept() {
		return $this->httpAccept;
	}

	public function getHttpMethod() {
		return $this->httpMethod;
	}

	public function getParam($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : null);
	}

	public function getOutput() {
		return $this->output;
	}

	public function getRawOutput() {
		return $this->rawOutput;
	}

	public function getSubUrl($fromPos) {
		$result = '';
		for ($i = $fromPos; $i < count($this->url); ++$i)
		{
			if ($i != $fromPos)
				$result .= '/';
			$result .= $this->url[$i];
		}

		return $result;
	}

	public function getUrlPart($depth) {
		if ($depth < 0 || $depth >= count($this->url))
			return null;

		return $this->url[$depth];
	}

	public function getUrlParam($paramName) {
		if (!isset($_GET[$paramName]))
			return null;

		return $_GET[$paramName];
	}

	public function hasError() {
		return ($this->httpError || $this->error);
	}

	public function setCacheControl($cacheControl) {
		$this->cacheControl = $cacheControl;
	}

	/**
	 * Sets an error result overwriting any other output except an http error.
	 * The error code and message will be returned as an data object '__ERROR__' containing 'code' and 'msg'.
	 * @see setHttpError
	 */
	public function setError($code, $msg) {
		$this->error = array('code' => $code, 'msg' => $msg);
	}

	public function setETag($eTag) {
		$this->eTag = $eTag;
	}

	public function setExpires($expire) {
		$this->$expire = $expire;
	}

	/**
	 * Sets an http error result overwriting any other output.
	 * If the method is called without a callback, this will be returned as a native http error. If a callback is
	 * provided, the http code will be 200, but the return value will be an data object '__HTTP_ERROR__' containing
	 * 'httpStatusCode' and 'msg'.
	 * @see setError
	 */
	public function setHttpError($httpStatusCode, $msg = null) {
		$this->httpError = array('httpStatusCode' => $httpStatusCode, 'msg' => $msg);
	}

	/**
	 * Sets the output data for the smart content output method.
	 * Output will be converted as required by the http accept header of the request. This function must only be called
	 * with empty raw output and raw content type.
	 * @param mixed $output
	 * @see setRawOutput
	 */
	public function setOutput($output) {
		if ($this->rawContentType !== null)
			throw new Exception('Cannot set output, because raw content type is already set. Use setRawOutput.');
		if ($this->rawOutput !== null)
			throw new Exception('Cannot set output, because raw output is already set.');

		$this->output = $output;
	}

	/**
	 * Sets the content type if the raw output method is used.
	 * This function must only be called if the output set by {@link setOutput} is empty.
	 * @param string $rawContentType Http content type, e.g. 'application/json'.
	 * @see setRawOutput
	 */
	public function setRawContentType($rawContentType) {
		if (!is_string($rawContentType))
			throw new Exception('$rawContentType must be a string.');
		if ($this->output !== null)
			throw new Exception('Cannot set raw content type, because output is already set with setOutput. '.
				'Use setRawOutput instead.');

		$this->rawContentType = $rawContentType;
	}

	/**
	 * Sets the raw output data.
	 * The output will be echoed to the client as received in this function. A content type may be defined by
	 * {@link setRawContentType}. This function must only be called if the output set by {@link setOutput} is empty.
	 * @param string $rawOutput
	 * @see setOutput.
	 */
	public function setRawOutput($rawOutput) {
		if (!is_string($rawOutput))
			throw new Exception('$rawOutput must be a string.');
		if ($this->output !== null)
			throw new Exception('Cannot set raw output, because output is already set.');

		$this->rawOutput = $rawOutput;
	}

	// Tools.

	/**
	 * Gets the default status message for an http status code.
	 * @param int $statusCode
	 * @return string
	 */
	public static function getStatusCodeMessage($statusCode) {
		$codes = Array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported'
		);

		return (isset($codes[$statusCode])) ? $codes[$statusCode] : '';
	}

	// Private Functions

	private function echoRaw($contentType, $content, $isJson, $httpStatusCode = null, $httpStatusMsg = null) {
		if (empty($_GET['callback'])) {
			if ($httpStatusCode && $httpStatusMsg)
				header('HTTP/1.1 '.$httpStatusCode.' '.$httpStatusMsg);
			else if ($httpStatusCode)
				header('HTTP/1.1 '.$httpStatusCode);

			header('Content-Type: '.$contentType);
			echo $content;
		}
		else {
			header('Content-Type: text/javascript');
			if ($isJson)
				echo($_GET['callback'].'('.$content.');');
			else
				echo($_GET['callback'].'('.json_encode($content).');');
		}
	}

	private function parseHttpAccept($httpAccept) {
		$result = array();
		$accepts = explode(',', str_replace(' ', '', strtolower($httpAccept)));
		foreach($accepts as $accept) {
			$def = explode(';', $accept);
			if (count($def) === 0)
				continue;

			$mediaType = explode('/', array_shift($def));
			if (count($mediaType) !== 2)
				continue;

			$quality = 1.0;
			while (($param = array_shift($def)) !== NULL) {
				$keyVal = explode('=', $param);
				if (count($keyVal) === 2 && $keyVal[0] === 'q') {
					$quality = (float)$keyVal[1];
					continue;
				}
			}

			$result[] = array('media-type' => $mediaType[0], 'subtype' => $mediaType[1], 'quality' => $quality);
		}

		return $result;
	}

	private function preferredContentType() {
		$preferredType = 'json';
		$preferredName = 'application/json';
		$preferredQuality = 0;
		foreach ($this->httpAccept as $accept) {
			$type = null;
			switch ($accept['media-type'].'/'.$accept['subtype']) {
				case 'application/json':
				case 'text/javascript':
					$type = 'json';
				break;
				case 'text/plain':
					$type = 'text';
				break;
			}
			if ($type !== null && $accept['quality'] > $preferredQuality) {
				$preferredType = $type;
				$preferredName = $accept['media-type'].'/'.$accept['subtype'];
				$preferredQuality = $accept['quality'];
			}
		}

		return array('type' => $preferredType, 'name' => $preferredName);
	}
}
