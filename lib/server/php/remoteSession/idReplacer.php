<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package Tools
 */

/**
 * @package Tools
 */
class IdReplacer {
    
	/**
	* In $data, replace field $replaceFrom with a tempid in field $replaceTo.
	* 
	* @param type $tempIdCreator TempIDGenerator (e.g. remoteSession) to use
	* @param type $replaceFrom e.g. teamid
	* @param type $replaceTo e.g. teamtempid
	* @param type $data Array or object containing field $replaceFrom
	* @param type $replaceAllInts Additionally, replace all fields containing a number>0 with their corresponding tempid.
	*/
	public static function replace(&$tempIdCreator, $replaceFrom, $replaceTo, &$data, $replaceAllInts=true) {
		foreach ($data as $key => &$value) {
			if ($replaceAllInts || $key == $replaceFrom){
				if (is_array($value) && isset($value[$replaceFrom])) {
					$value[$replaceTo] = $tempIdCreator->getTempId($value[$replaceFrom]);
					if($replaceTo != $replaceFrom)
						unset($value[$replaceFrom]);
				} else if (is_object($value) && property_exists($value, $replaceFrom)) {
					$value->$replaceTo = $tempIdCreator->getTempId($value->$replaceFrom);
					if($replaceTo != $replaceFrom)
						unset($value->$replaceFrom);
				} else if (is_int($value) or (int) $value > 0) {
					$data[$key] = $tempIdCreator->getTempId($value);
				}
                        }
		}
	}

	public static function remove($removeKey, &$data) {
		foreach ($data as $key => &$value) {
			if (is_array($value) && isset($value[$removeKey]))
				unset($value[$removeKey]);
			else if (is_object($value) && property_exists($value, $removeKey))
				unset($value->$removeKey);  
		}
	}

	public static function replaceWithTempIds(&$tempIdCreator, $replaceKey, $data) {
		$newData = array();
		foreach ($data as $key => &$value) {
			$value['id'] = $tempIdCreator->getTempId($value['id']);
			$newData[] = $value;
		}
		return $newData;
	}
}
