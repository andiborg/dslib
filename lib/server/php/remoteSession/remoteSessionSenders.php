<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package RemoteSession
 */

/**
 * An IRemoteSessionSender is called by a {@link RemoteSession} to send session change data to the remote host.
 */
interface IRemoteSessionSender
{
	/**
	 * @param $sessionId
	 * @param array $changes Array of changes as returned by {@link RemoteSession}.
	 */
	function send($sessionId, $changes);
}

/**
 * HttpRemoteSessionSender uses a http post to send session change data to a remote host.
 * The format of the change data is compatible to the {@link HttpRemoteSessionReceiver}. 
 */
class HttpRemoteSessionSender implements IRemoteSessionSender {
	private $host;
	private $port;
	private $function;
	private $url;

	/**
	 * Initializes the sender with information on contacting the remote host.
	 * Example parameters: ('www.example.com', 1024, '/receiver.php').
	 * @param string $host
	 * @param string $port
	 * @param string $function
	 */
	public function __construct($host, $port, $function) {
		if (!is_string($host) || !$port || !is_string($function))
			throw new Exception("host, port and function must be strings.");
		$this->url = ''; ($port===443)?$this->url .= 'https://':$this->url .= 'http://'; $this->url .= $host . $function;
	}

	/**
	 * @param $sessionId
	 * @param array $changes Array of changes as returned by {@link RemoteSession}.
	 */
	public function send($sessionId, $changes) {
            $content = serialize(array('sessionId' => $sessionId, 'changes' => $changes));
            $c = curl_init();
            curl_setopt($c, CURLOPT_URL, $this->url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_POST, true);
            curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($content)));
            curl_setopt($c, CURLOPT_POSTFIELDS, $content);
            $result = curl_exec($c);
            $httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
            if ($result === FALSE || $httpcode < 200 || $httpcode >= 300) {
            	throw new Exception("Request to ".$this->url." failed with status code $httpcode and message '$result'!");
            }           
	}
}
