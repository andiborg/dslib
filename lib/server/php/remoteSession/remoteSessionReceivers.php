<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * @package RemoteSession
 */

/**
 * HttpRemoteSessionReceiver can be used to receive the output of the {@link HttpRemoteSessionSender}.
 */
class HttpRemoteSessionReceiver {
	private $remoteSessionFactory;

	/**
	 * @param RemoteSessionFactory $remoteSession
	 */
	public function __construct(&$remoteSessionFactory) {
		$this->remoteSessionFactory = $remoteSessionFactory;
	}

	public function receive() {
		$data = file_get_contents("php://input");
		$changeInfo = unserialize($data);
		try {
			$this->remoteSessionFactory->loadAndUpdate($changeInfo['sessionId'], $changeInfo['changes']);
		}
		catch (Exception $e) {
			error_log($e);
			header('HTTP/1.1 400 Bad Request.');
			echo $e->getMessage();
		}
	}
}
