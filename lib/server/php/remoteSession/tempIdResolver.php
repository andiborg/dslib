<?php

/**
 * Class which handles requests of the ID-Resolver.
 * 
 * Usage: In the server-side script which handles requests of the ID-Resolver,
 * instatiate an object of this class. A user-defined function that resolves 
 * Temp-ID / subject pairs has to be passed as argument (see
 * description of constructor for details). E.g:
 * 
 * $resolver = new TempIdResolver('resolve');
 * 
 * Requests are served by calling method resolve():
 * 
 * $resolver->resolve();
 * 
 * 
 * @author borg
 *
 */
class TempIdResolver {
	
	private $resolveFunction;
	
	/** Create a new resolver.
	 * 
	 * @param $resolveFunction: A user-defined function which takes as arguments a subject name
	 * and a Temp-ID (both strings) and returns the appropriate resolved value. Can be passed as
	 * string (function name) or as an anonymous function.
	 */
	public function __construct($resolveFunction) {
		$this->resolveFunction = $resolveFunction;
	}
	
	/**
	 * Handle a request of the JS-ID-Resolver.
	 * Reads $_REQUEST and proceeds with the steps neccessary for Temp-ID resolution:
	 *  - reads and parses input json
	 *  - loops over input data and collects the resolved values
	 *  - echoes the response as JSON or JSONP.
	 *  
	 * JSONP is used if a parameter 'callback' has been sent. The response is then in the format
	 * $_REQUEST['callback'].'('.$data.')'
	 * where $data is the output as JSON.
	 */
	public function resolve() {
		// save name of external resolve function as variable
		// $this->resolveFunction would try to find a method "resolveFunction"
		// instead of a function whose name is the value of $this->resolveFunction
		$functionName = $this->resolveFunction;
		
		// Auskommentierter Code ist für Cross-Origin-Resource-Sharing
		// (wird von IE noch nicht unterstützt)
				
// 		header('Access-Control-Allow-Origin: *');
// 		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
// 		header('Access-Control-Max-Age: 1000');
// 		if(array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
// 			header('Access-Control-Allow-Headers: '
// 					. $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
// 		} else {
// 			header('Access-Control-Allow-Headers: *');
// 		}
		
// 		if("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
// 			exit(0);
// 		}
		
		
		if (!isset($_REQUEST['data'])) {
			header('HTTP/1.1 400 Invalid request.');
			return;
		}
		
		$data = json_decode($_REQUEST['data'], true);
		$result = array();
		
		if (isset($data['subjects']) && is_array($data['subjects']))
			foreach ($data['subjects'] as $subject => $tempIds)
			if (is_array($tempIds))
			foreach ($tempIds as $tempId)
			$result[$subject][$tempId] = $functionName($subject, $tempId);
		
		if (isset($data['tempIds']) && is_array($data['tempIds']))
			foreach ($data['tempIds'] as $tempId => $subjects)
			if (is_array($subjects))
			foreach ($subjects as $subject)
			$result[$subject][$tempId] = $functionName($subject, $tempId);
		
		$jsonString = json_encode($result);
		// Check for JSONP-Request
		if (isset($_REQUEST['callback']))
			echo $_REQUEST['callback']."(".$jsonString.")";
		else
			echo json_encode($result);
				
	}
}