<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/** 
 * @package RemoteSession
 */

$libDir = dirname(__FILE__);
require_once $libDir.'/remoteSessionFactory.php';
require_once $libDir.'/remoteSessionReceivers.php';
require_once $libDir.'/remoteSessionSenders.php';
require_once $libDir.'/remoteSessionStores.php';

/**
 * RemoteSession represents a single, specific remote session.
 * RemoteSessions should not be initialised directly, but be created using the {@link RemoteSessionFactory}. A session
 * object can only be loaded once. After the session is destroyed, the object is useless. 
 */
class RemoteSession {
	private $notSent = array();
	private $session = array();
	/** Reverse mapping ID -> Temp-ID for efficient retreival of Temp-IDs */
	private $tempIdById = array();
	private $sessionId = null;

	private $remoteSessionSender = null;
	private $remoteSessionStore = null;

	/**
	 * Do not use this constructor, use the {@link RemoteSessionFactory} instead.
	 * @param IRemoteSessionStore &$remoteSessionStore
	 * @param IRemoteSessionSender &$remoteSessionSender
	 * @param mixed $sessionId Id of the session to load or create.
	 * @param bool $remoteCreate If the session cannot be loaded, should the create action be transferred to the 
	 * RemoteSessionSender?
	 */
	public function __construct(&$remoteSessionStore, &$remoteSessionSender, $sessionId, $remoteCreate = true) {
		if (!($remoteSessionStore instanceof IRemoteSessionStore))
			throw new Exception('remoteSessionStore must be an instance of IRemoteSessionStore.');
		if ($remoteSessionSender !== null && !($remoteSessionSender instanceof IRemoteSessionSender))
			throw new Exception('remoteSessionSender must be null or an instance of IRemoteSessionSender.');

		$this->remoteSessionStore = $remoteSessionStore;
		$this->remoteSessionSender = $remoteSessionSender;

		if (!$this->innerLoad($sessionId)) {
			$this->innerCreate($sessionId);
			$this->notSent[] = array('act' => 'create');
			if ($remoteCreate) {
				$this->save();
			} else {
				$this->update($this->getSessionId(), $this->notSent);
				$this->notSent = array();
			}
		}
	}

	public function __destruct() {
		$notSentCount = count($this->notSent); 
		if ($notSentCount != 0)
			throw new Exception("RemoteSession destructed with $notSentCount not sent actions.");
	}

	/**
	 * Deletes a key from the session.
	 * @param mixed $key
	 */
	public function delete($key) {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');
		if (!$key)
			throw new Exception("Empty parameter 'key'.");

		$this->innerDelete($key);
		$this->notSent[] = array('act' => 'del', 'key' => $key);
	}

	/**
	 * Destroys the session and immediately sends the change.
	 * This triggers two remote actions. All changes are stored localy and remotely, then the destroy action is send
	 * and the session is destroyed localy. This allows the remote party to see and react to all changes. After
	 * destroying the session, the RemoteSession object can no longer be used.
	 */
	public function destroy() {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');

		$this->save();

		$this->notSent[] = array('act' => 'destroy');
		if ($this->remoteSessionSender !== null)
			$this->remoteSessionSender->send($this->getSessionId(), $this->notSent);
		$this->innerDestroy();
		$this->notSent = array();
	}

	/**
	 * @param mixed $key
	 * @return mixed Value associated with the key or null, if the key is not set.
	 */
	public function get($key) {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');
		if (!$key)
			throw new Exception("Empty parameter 'key'.");

		return isset($this->session[$key]) ? $this->session[$key] : null;
	}

	/**
	 * @param mixed $tempId
	 * @return mixed Id, that is associated with the temporary id or null.
	 */
	public function getId($tempId) {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');
		if (!$tempId)
			throw new Exception("Empty parameter 'tempId'.");

		return $this->get('_tempIds/'.$tempId);
	}

	/** 
	 * @return mixed Id of the loaded session or null, if no session is loaded.
	 */
	public function getSessionId() {
		return $this->sessionId;
	}

	/**
	 * @param mixed $id
	 * @return string Temporary id that is associated with the $id. 
	 */
	public function getTempId($id) {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');
		if (!$id)
			throw new Exception("Empty parameter 'id'.");

		// Look up Temp-ID in table, if there is none, create it
 		if (isset($this->tempIdById[$id]))
 			return $this->tempIdById[$id];
		

		$tempId = self::randomString();
		$this->set('_tempIds/'.$tempId, $id);
		$this->tempIdById[$id] = $tempId;

		return $tempId;
	}

	/**
	 * @return mixed Length of timeout in seconds, or null, if timeouts are not supported or used.
	 */
	public function getTimeoutLengthS() {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');

		if (!$this->remoteSessionStore instanceof ITimeoutRemoteSessionStore)
			return null;
		return $this->remoteSessionStore->getTimeoutLengthS($this->getSessionId());
	}

	/**
	 * @return mixed Time of timeout as unix timestamp, or null, if timeouts are not supported or used.
	 */
	public function getTimeoutTime() {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');

		if (!$this->remoteSessionStore instanceof ITimeoutRemoteSessionStore)
			return null;
		return $this->remoteSessionStore->getTimeoutTime($this->getSessionId());
	}

	/**
	 * Saves the session remotely and locally.
	 * For best performance save should be called only once per server access. 
	 */
	public function save() {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');

		if (empty($this->notSent))
			return;

		if ($this->remoteSessionSender !== null)
			$this->remoteSessionSender->send($this->getSessionId(), $this->notSent);
		$this->update($this->getSessionId(), $this->notSent);
		$this->notSent = array();
	}

	/**
	 * @param mixed $key
	 * @param mixed $value
	 */
	public function set($key, $value) {
		if (!$this->getSessionId())
			throw new Exception('Session is not loaded.');
		if (!$key)
			throw new Exception("Empty parameter 'key'.");

		$this->innerSet($key, $value);
		$this->notSent[] = array('act' => 'set', 'key' => $key, 'val' => $value);
	}

	/**
	 * Do not use this function, it is meant to be used in the remote session update process.
	 * @param mixed $sessionId Id of the session to update. This must be identical to the id of the loaded session.
	 * @param array $changes List of change actions that have to be performed.
	 */
	public function update($sessionId, $changes) {
		if (!$sessionId)
			throw new Exception("Empty parameter 'sessionId'.");
		if ($this->getSessionId() !== $sessionId )
			throw new Exception("Session id missmatch in update.");
		if (!is_array($changes))
			throw new Exception("Changes must be an array.");

		if (empty($changes))
			return;

		// Check for special session actions.
		$loopMore = true;
		switch ($changes[0]['act']) {
			case 'create':
				$this->innerCreate($sessionId);
				array_shift($changes);
				break;
			case 'destroy':
				if ($this->innerLoad($sessionId))
					$this->innerDestroy();
				array_shift($changes);
				$loopMore = false;
				break;
		}

		if (!$loopMore)
			return;

		// Loop over other changes.
		$this->innerLoad($sessionId);
		foreach ($changes as $change) {
			switch ($change['act']) {
				case 'del':
					$this->innerDelete($change['key']);
					break;
				case 'set':
					$this->innerSet($change['key'], $change['val']);
					break;
				default:
					throw new Exception("Action '{$change['act']}' is unknown or not supported at this position."); 
			}
		}
		$this->remoteSessionStore->save($this->getSessionId(), $this->session);
	}

	// Private functions.

	private function innerCreate($sessionId) {
		$this->sessionId = $sessionId;
		$this->session = array();
		$this->tempIdById = array();
	}

	private function innerDelete($key) {
		// If the item to delete is a Temp-ID, the reverse mapping is also deleted.
		// Check for type to prevent message "Illegal offset type".
		$type = gettype($this->session[$key]);
		if ($type == "integer" || $type == "string") 
			unset($this->tempIdById[$this->session[$key]]);
		unset($this->session[$key]);
	}

	private function innerDestroy() {
		$this->remoteSessionStore->delete($this->sessionId);
		$this->sessionId = null;
		$this->session = array();
		$this->tempIdById = array();
	}

	private function innerLoad($sessionId) {
		$sessionData = $this->remoteSessionStore->load($sessionId);
		if ($sessionData === null)
			return false;

		$this->session = &$sessionData;
		// Find Temp-IDs in Session and add reverse mapping (ID -> Temp-ID) to cache 
		foreach ($this->session as $key => $value) {
			if (strpos($key, '_tempIds/') === 0) {
				$tempId = substr($key, 9, strlen($key) - 9);
				$this->tempIdById[$value] = $tempId;
			}
		}
		$this->setSessionId($sessionId);
		return true;
	}

	private function innerSet($key, $value) {
		// No setting of $this->tempIdById (performed in getTempId)
		$this->session[$key] = $value;
	}

	private static function randomString($length = 32,
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
	{
		$charsLength = (strlen($chars) - 1);
		$string = '';
		for ($i = 0; $i < $length; ++$i)
			$string .= $chars{mt_rand(0, $charsLength)};
		return $string;
	}

	private function setSessionId($sessionId) {
		$this->sessionId = $sessionId;
	}
}
