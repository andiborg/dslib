<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/**
 * Contains different classes that store remote sessions.
 * @package RemoteSession
 */

/**
 * An IRemoteSessionStore is used by {@link RemoteSession} to locally load, save and delete a session.
 */
interface IRemoteSessionStore {

	/**
	 * @param mixed $sessionId
	 */
	function delete($sessionId);

	/**
	 * @param mixed $sessionId
	 * @return bool
	 */
	function exists($sessionId);

	/**
	 * This function is used by the {@link RemoteSessionFactory} to find a session by temporary id.
	 * Implementation of this function is optional. Stores not supporting this function should throw an exception.
	 * @param mixed $tempId
	 * @return mixed Id of a session, that contains the tempId. 
	 */
	function getSessionId($tempId);

	/**
	 * @param mixed $sessionId
	 * @return array Data of the session or null if a session with the id could not be found.
	 */
	function load($sessionId);

	/**
	 * @param mixed $sessionId
	 * @param array &$sessionData
	 */
	function save($sessionId, &$sessionData);
}

interface ITimeoutRemoteSessionStore extends IRemoteSessionStore {
	/** 
	 * @return int Length of timeout in seconds.
	 */
	function getTimeoutLengthS($sessionId);
	
	/**
	 * getTimeoutTime returns the time at which the session will time out.
	 * @return Time of timeout as unix timestamp.
	 */
	function getTimeoutTime($sessionId);

	/**
	 * Sets the listener function, that is called, when a session times out. 
	 */
	function setSessionTimeoutListener($listener);
}

/**
 * FileRemoteSessionStore uses a file system directory to store sessions.
 * Supports timeout and (optionally) finding session ids by temporary ids.
 */
class FileRemoteSessionStore implements ITimeoutRemoteSessionStore {
	private $dir = null;
	private $timeoutSecs;
	private $sessionTimeoutListener;
	private $supportFindSessionId;

	/**
	 * @param string $dir
	 * @param int $timeoutSecs
	 * @param bool $supportFindSessionId Set to true to enable finding session ids by temporary ids.
	 */
	public function __construct($dir, $timeoutSecs = 3600, $supportFindSessionId = false) {
		if (!is_string($dir))
			throw new Exception('dir must be a string.');
		$this->dir = $dir;
		$this->timeoutSecs = $timeoutSecs;
		$this->supportFindSessionId = $supportFindSessionId;
	}

	/**
	 * @param mixed $sessionId
	 */
	public function delete($sessionId) {
		$sessionFileName = $this->getSessionFilename($sessionId);
		if (file_exists($sessionFileName) && unlink($sessionFileName) === false)
			throw new Exception("Could not delete file '$filePath'.");
	}

	/**
	 * @param mixed $sessionId
	 * @return bool
	 */
	public function exists($sessionId) {
		$sessionFileName = $this->getSessionFilename($sessionId);
		return $this->fileValid($sessionFileName);
	}

	/**
	 * getSessionId finds the id of a session that contains a specific temporary id, if function was enabled.
	 * This function is used by the {@link RemoteSessionFactory} to find a session by temporary id.
	 * If finding session ids is not enabled in the constructor, this function throws an exception.
	 * @param mixed $tempId
	 * @return mixed Id of a session, that contains the tempId. 
	 */
	public function getSessionId($tempId) {
		if (!$this->supportFindSessionId)
			throw new Exception('Finding sessions by temp id is not supported. Set to true in constructor.');

		$tempIdFileName = $this->getTempIdFilename($tempId);
		if ($this->fileValid($tempIdFileName) === false)
			return null;

		$sessionId = file_get_contents($tempIdFileName);
		if ($sessionId === false)
			throw new Exception("Could not read file '$tempIdFileName'");

		return $sessionId;
	}

	/** 
	 * @return int Length of timeout in seconds.
	 */
	public function getTimeoutLengthS($sessionId) {
		return $this->timeoutSecs;
	}

	/**
	 * getTimeoutTime returns the time at which the session will time out.
	 * @return Time of timeout as unix timestamp.
	 */
	public function getTimeoutTime($sessionId) {
		$sessionFileName = $this->getSessionFilename($sessionId);

		$stats = stat($sessionFileName);
		if ($stats === false)
			throw new Exception("Filesystem information for '$filePath' could not be accessed.");
		if ($stats['mtime'] <= (time() - $this->timeoutSecs))
			return null;

		return $stats['mtime'] + $this->timeoutSecs;
	}

	/**
	 * @param mixed $sessionId
	 * @return array Data of the session or null if a session with the id could not be found.
	 */
	public function load($sessionId) {
		$sessionFileName = $this->getSessionFilename($sessionId);
		if ($this->fileValid($sessionFileName) === false)
			return null;
		// Obtain a lock to prevent that file is written to at the same moment
		// (Write log in save() is not sufficient, at least on Linux)
		$fh = fopen($sessionFileName, "r");
 		if (flock($fh, LOCK_SH)) {		
			$data = unserialize(file_get_contents($sessionFileName));
 			flock($fh, LOCK_UN);
 			fclose($fh);
			return $data;
 		} else {
 			fclose($fh);			
 			throw new Exception("Could not get file lock for RemoteSession file");
 		}
	}

	/**
	 * Saves the session data to file system and checks for old session files.
	 * Checking for old files is done with a probability of 1/200.
	 * @param mixed $sessionId
	 * @param array &$sessionData
	 */
	public function save($sessionId, &$session) {
		$sessionFileName = $this->getSessionFilename($sessionId);
//		error_log("Started saving RemoteSession with id $sessionId, containing ".count($session)." values");
		file_put_contents($sessionFileName, serialize($session), LOCK_EX);
//		error_log("Finished saving RemoteSession with id $sessionId");
		
		if ($this->supportFindSessionId)
			foreach ($session as $key => &$value)
				if (strpos($key, '_tempIds/') === 0) {
					$tempIdFileName = $this->getTempIdFilename(substr($key, 9, strlen($key) - 9));
					file_put_contents($tempIdFileName, $sessionId);
				}

		clearstatcache();
		if (rand(0, 199) == 199)
			$this->checkFilesValidity();
	}

	function setSessionTimeoutListener($listener) {
		if (empty($listener))
			throw new Exception('listener is empty.');
		if (!is_callable($listener))
			throw new Exception('listener is not callable.');

		$this->sessionTimeoutListener = $listener;
	}

	// Private Functions.

	private function fileValid($filePath) {
		if (!is_file($filePath))
			return false;

		$stats = stat($filePath);
		if ($stats === false)
			throw new Exception("Filesystem information for '$filePath' could not be accessed.");
		if ($stats['mtime'] > (time() - $this->timeoutSecs))
			return true;
		if (!empty($this->sessionTimeoutListener)) {
			$sessionId = $this->getSessionIdFromFilePath($filePath);
			$sessionData = unserialize(file_get_contents($filePath));
			call_user_func($this->sessionTimeoutListener, $sessionId, $sessionData);
		}
		if (unlink($filePath) === false)
			throw new Exception("Could not delete file '$filePath'.");

		return false;
	}

	private function checkFilesValidity() {
		$fileNames = scandir($tempIdFileName = $this->dir);
		foreach ($fileNames as $fileName)
			try { $this->fileValid($tempIdFileName = $this->dir.'/'.$fileName); } catch (Exception $e) {}
	}

	private function getSessionFilename($sessionId) {
		return $this->dir.'/session_'.$sessionId;
	}

	private function getSessionIdFromFilePath($filePath) {
		return substr($filePath, strlen($this->dir) + 9);
	}

	private function getTempIdFilename($tempId) {
		return $this->dir.'/tempid_'.$tempId;
	}
}

/**
 * SessionRemoteSessionStore stores remote sessions in the local session object.
 * If both ends of the remote session are used on a single computer, this store can be used in a
 * {@link RemoteSessionFactory} without a sender. The 'simple'-Demo uses this trick. Implementations that use a central
 * sql server to exchange remote session information could work in the same way.
 */
class SessionRemoteSessionStore implements IRemoteSessionStore {

	/** 
	 * @param mixed $sessionId
	 */
	function delete($sessionId) {
		unset($_SESSION['remoteSession'][$sessionId]);
	}

	/**
	 * @param mixed $sessionId
	 * @return bool
	 */
	function exists($sessionId) {
		return isset($_SESSION['remoteSession'][$sessionId]);
	}

	/**
	 * This function is used by the {@link RemoteSessionFactory} to find a session by temporary id.
	 * Implementation of this function is optional. Stores not supporting this function should throw an exception.
	 * @param mixed $tempId
	 * @return mixed Id of a session, that contains the tempId. 
	 */
	function getSessionId($tempId) {
		throw new Exception('Not supported.');
	}

	/**
	 * @param mixed $sessionId
	 * @return array Data of the session or null if a session with the id could not be found.
	 */
	function load($sessionId) {
		return $_SESSION['remoteSession'][$sessionId];
	}

	/**
	 * @param mixed $sessionId
	 * @param array &$sessionData
	 */
	function save($sessionId, &$session) {
		$_SESSION['remoteSession'][$sessionId] = $session;
	}

}
