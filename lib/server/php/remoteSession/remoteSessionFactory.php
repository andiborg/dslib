<?php
/*
 * Copyright (c) 2010 René Brüntrup, Martin Lablans, Frank Ückert 
 * Licensed under the MIT X11 License (see LICENSE.txt).
 */

/** 
 * @package RemoteSession
 */

/**
 * A RemoteSessionFactory is used to create, load and update {@link RemoteSession} instances.
 * Initialise this class with the appropriate stores and senders and use it to load or create sessions.
 * A {@link IRemoteSessionReceiver} uses instances of this class to update sessions.
 */
class RemoteSessionFactory {
	private $remoteSessionSender = null;
	private $remoteSessionStore = null;

	/**
	 * @param IRemoteSessionStore &$remoteSessionStore
	 * @param IRemoteSessionSender &$remoteSessionSender
	 */
	public function __construct(&$remoteSessionStore, &$remoteSessionSender = null) {
		if (!($remoteSessionStore instanceof IRemoteSessionStore))
			throw new Exception('remoteSessionStore must be an instance of IRemoteSessionStore.');
		if ($remoteSessionSender !== null && !($remoteSessionSender instanceof IRemoteSessionSender))
			throw new Exception('remoteSessionSender must be null or an instance of IRemoteSessionSender.');

		$this->remoteSessionStore = $remoteSessionStore;
		$this->remoteSessionSender = $remoteSessionSender;
	}

	/**
	 * Creates a new session with the specified id.
	 * The session is immidiately created locally and remotely.
	 * @param mixed $sessionId
	 * @exception Throws an Exception, if a session with the id exists.
	 * @return remoteSession
	 */
	public function create($sessionId) {
		if (!$sessionId)
			throw new Exception("Empty parameter 'sessionId'.");
		if ($this->remoteSessionStore->exists($sessionId))
			throw new Exception("Session with id '$sessionId' already exists.");

		return new RemoteSession($this->remoteSessionStore, $this->remoteSessionSender, $sessionId);
	}

	/**
	 * @param mixed $sessionId
	 * @return remoteSession Loaded session or null if session does not exist.
	 */
	public function load($sessionId) {
		if (!$sessionId)
			throw new Exception("Empty parameter 'sessionId'.");
		if (!$this->remoteSessionStore->exists($sessionId))
			return null;

		$session = new RemoteSession($this->remoteSessionStore, $this->remoteSessionSender, $sessionId);
		return $session;
	}

	/**
	 * This session is called by a {@link IRemoteSessionReceiver} to update the session.
	 * @param mixed $sessionId
	 * @param array $changes List of change actions as sent by the {@link RemoteSession}.
	 */
	public function loadAndUpdate($sessionId, $changes) {
		if (!$sessionId)
			throw new Exception("Empty parameter 'sessionId'.");
		if (!is_array($changes))
			throw new Exception("Changes must be an array.");

		if (empty($changes))
			return;

		$session = null;
		if ($changes[0]['act'] === 'create') {
			$this->remoteSessionStore->delete($sessionId);
			$session = new RemoteSession(
				$this->remoteSessionStore, $this->remoteSessionSender, $sessionId, false);
			array_shift($changes);
		} else
			$session = $this->load($sessionId);

		if ($session === null && $changes[0]['act'] !== 'destroy') {
			error_log("Unknown session $sessionId");
			throw new Exception("Received changes for unknown session: $sessionId.");
		}

		$session->update($sessionId, $changes);

		return $session;
	}

	/**
	 * @param mixed $tempId
	 * @return remoteSession Loaded session if a session containing the temp id could be loaded, null otherwise.
	 */
	public function loadByTempId($tempId) {
		if (!$tempId)
			throw new Exception("Empty parameter 'tempId'.");

		$sessionId =  $this->remoteSessionStore->getSessionId($tempId);
		if (!$sessionId)
			return null;

		return $this->load($sessionId, false);
	}
}
